package us.sleepybear.minecraft.blockinfo.event;

import cn.nukkit.Player;
import cn.nukkit.Server;
import cn.nukkit.block.Block;
import cn.nukkit.event.Listener;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.player.PlayerJoinEvent;
import cn.nukkit.event.player.PlayerMoveEvent;
import cn.nukkit.event.player.PlayerQuitEvent;
import cn.nukkit.item.Item;
import cn.nukkit.level.Position;
import cn.nukkit.utils.Config;
import cn.nukkit.utils.TextFormat;
import us.sleepybear.minecraft.blockinfo.BlockInfoPlugin;
import us.sleepybear.minecraft.blockinfo.Util;
import java.util.HashMap;
import java.util.Map;

public class BIEventHandler implements Listener {
    private BlockInfoPlugin plugin;
    private Map<String, Position> blockMap;
    private Map<String, Boolean> enableMap;
    private static int viewDisatnce = 14;


    public BIEventHandler(BlockInfoPlugin plugin) {
        this.plugin = plugin;
        this.blockMap = new HashMap<String, Position>();
        this.enableMap = new HashMap<String, Boolean>();
    }

    @EventHandler
    public void onPME(PlayerMoveEvent ev) {
        if(ev.isCancelled() || !ev.getPlayer().hasPermission("blockinfo.enable")) return;

        Block b = ev.getPlayer().getTargetBlock(viewDisatnce);
        if(b == null) return; // not using Block.isNull() because we are ok with Block.AIR
        String pname = ev.getPlayer().getName();
        if(enableMap.containsKey(pname) && enableMap.get(pname) == false) return;

        if( blockMap.containsKey(pname) && b == blockMap.get(pname)) { return; }
        blockMap.put(pname,b);
        Item hand = ev.getPlayer().getInventory().getItemInHand();

        Util.sendText(ev.getPlayer(), new StringBuilder()
                                            .append("TPS: ").append(Server.getInstance().getTicksPerSecondAverage())
                                            .append(" Block Info: ")
                                            .append(b.getName())
                                            .append(" (").append(b.getId()).append(":").append(b.getDamage()).append(")")
                                            .append(" Pos: (").append(b.getLocation().getX()).append(", ")
                                            .append(b.getLocation().getY()).append(", ")
                                            .append(b.getLocation().getZ()).append(")")
                                           .toString(),
                new StringBuilder().append("Item in Hand: ").append(hand == null ? "null" : hand.getName())
                        .append(" (").append(hand.getId()).append(":").append(hand.getDamage()).append(")").toString());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent ev) {
        try {
            if(enableMap.containsKey(ev.getPlayer().getName() )) {
                Config data = this.plugin.getDataFile();
                data.set(ev.getPlayer().getUniqueId().toString(),enableMap.get(ev.getPlayer().getName() ));
                data.save();
            }
            blockMap.remove(ev.getPlayer().getName());
            enableMap.remove(ev.getPlayer().getName());
        } catch (Exception e) {
            this.plugin.getLogger().error("Error processing OnQuit: " + e.getMessage());
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent ev) {
        try {
            Config data = this.plugin.getDataFile();
            String p = ev.getPlayer().getUniqueId().toString();

            if (data.getKeys().contains(p)) {
                enableMap.put(ev.getPlayer().getName(), data.getBoolean(p));
            } else {
                enableMap.put(ev.getPlayer().getName(), false);
            }
        } catch (Exception e) {
            this.plugin.getLogger().error("Error processing onJoin: " + e.getMessage());
        }
    }

    public void toggle(Player player) {
        if(enableMap.containsKey(player.getName())) {
            if(enableMap.get(player.getName())) {
                enableMap.put(player.getName(), false);
                player.sendMessage("BlockInfo has been " + TextFormat.DARK_RED +"disabled");
            } else {
                enableMap.put(player.getName(), true);
                player.sendMessage("BlockInfo has been " + TextFormat.GREEN +"enabled");
            }
        }
    }

    public void enable(Player player) {
        enableMap.put(player.getName(),true);
        player.sendMessage("BlockInfo has been " + TextFormat.GREEN +"enabled");
    }

    public void disable(Player player) {
        enableMap.put(player.getName(), false);
        player.sendMessage("BlockInfo has been " + TextFormat.DARK_RED +"disabled");
    }

    public void clear() {
        try {
            Config data = this.plugin.getDataFile();
            for (String key : enableMap.keySet()) {
                data.set(this.plugin.getServer().getPlayer(key).getUniqueId().toString(), enableMap.get(key));
            }
            data.save();
            this.enableMap = null;
            this.blockMap = null;
        } catch (Exception e) {
            this.plugin.getLogger().error("Error when saving player data: " + e.getMessage());
        }
    }

}
