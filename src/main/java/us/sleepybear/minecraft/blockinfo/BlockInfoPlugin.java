package us.sleepybear.minecraft.blockinfo;

import cn.nukkit.Player;
import cn.nukkit.command.Command;
import cn.nukkit.command.CommandSender;
import cn.nukkit.plugin.PluginBase;
import cn.nukkit.utils.Config;
import cn.nukkit.utils.TextFormat;
import us.sleepybear.minecraft.blockinfo.event.BIEventHandler;

import java.util.Arrays;

public class BlockInfoPlugin extends PluginBase {
    private BIEventHandler handler;

    @Override
    public void onEnable() {
        this.handler = new BIEventHandler(this);
        this.getServer().getPluginManager().registerEvents(handler, this);
        this.getLogger().info(TextFormat.GREEN + "BlockInfo Loaded!");
    }

    @Override
    public void onDisable() {
        this.handler.clear();
        this.handler = null;
        this.getLogger().info(TextFormat.RED + "BlockInfo DISABLED!");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        this.getLogger().debug("Processing " + command.getName() + "command with args: " + Arrays.toString(args));

        switch(command.getName().toLowerCase()) {
            case "blockinfo":
            case "bi":
                if(args.length > 1) {
                    //sender.sendMessage(command.getUsage()); // Don't have to send usage if we return false
                    return false;
                }
                if(args.length == 0) {
                    if(sender.isPlayer()) {
                        this.handler.toggle((Player) sender);
                    }
                    break;
                }

                switch (args[0].toLowerCase()) {
                    case "on":
                    case "enable":
                    case "true":
                        if(sender.isPlayer()) {
                            this.getLogger().info("Enabling BlockInfo");
                            this.handler.enable((Player) sender);
                            return true;
                        }
                        break;
                    case "off":
                    case "disable":
                    case "false":
                        if(sender.isPlayer()) {
                            this.getLogger().info("Disabling BlockInfo");
                            this.handler.disable((Player) sender);
                            return true;
                        }
                        break;
                    default:
                        return false;
                }
            default:
                return false;
        }
        return true;
    }

    public Config getDataFile () {
        return new Config(this.getDataFolder() + "/data.json");
    }

}
